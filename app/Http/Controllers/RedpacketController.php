<?php

namespace App\Http\Controllers;

use App\Models\Redpacket;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class RedpacketController extends Controller
{
    public function sendredpacket(Request $request)
{
    //param amount,quantity,random,user_id

        $users = User::inRandomOrder()->limit($request->quantity)
            ->whereNotIn('users.id', [$request->user_id])
            ->whereRaw("id not in (select user_id from transactions)")
            ->get();

    $redpacket_id = "";
    $msg="";

        $qty = sizeof($users);
        if ($qty < $request->quantity) {
            $status = "Failed.";
            $msg = "Only left $qty user who are not receive redpacket.";

        } else {

            //insert into transaction table

            $redpacket = Redpacket::create([
                'user_id' => $request->user_id,
                'amount' => $request->amount,
                'quantity' => $request->quantity,
                'random' => $request->random
            ]);

            $redpacket->save();


            //random is true
            if ($request->random == 1) {

                $lastperson = 0;
                $tot_amt = 0;

                foreach ($users as $user) {
                    //last person got all balance

                    if ($lastperson == ($request->quantity - 1)) {
                        $rcv_amount = $request->amount - $tot_amt;
                    } else {
                        //-1 at least last person not get 0
                        $rcv_amount = rand(0.01, ($request->amount-1)-$tot_amt);
                        $tot_amt = $tot_amt + $rcv_amount;
                    }


                    $trans = Transaction::create([
                        'user_id' => $user->id,
                        'amount_receive' => $rcv_amount,
                        'redpacket_id' => $redpacket->id

                    ]);

                    $trans->save();
                    $status = "Success";
                    $lastperson++;
                    $redpacket_id = $redpacket->id;
                }


            } else {
                //random is false

                $rcv_amount = $request->amount / $request->quantity;

                foreach ($users as $user) {
                    $trans = Transaction::create([
                        'user_id' => $user->id,
                        'amount_receive' => $rcv_amount,
                        'redpacket_id' => $redpacket->id

                    ]);

                    $trans->save();
                    $status = "Success";
                    $redpacket_id = $redpacket->id;

                }

            }


        }

    $return_sts = ["status"=>$status,"redpacket_id"=>$redpacket_id,"msg"=>$msg];

    return $return_sts;

}

    public function receiveredpacket(Request $request){
        //parameter redpacket_id,user_id

        $data = Transaction::where("redpacket_id",$request->redpacket_id)
            ->where("user_id",$request->user_id)->first();

        if($data){
            $status = "Success";
            $amount = number_format((float)$data->amount_receive, 2, '.', '');
            $msg = "";
        }else{
            $status = "Failed";
            $msg="Sorry.No Red Packet For you";
            $amount = 0;
        }

        $return_sts = ["status"=>$status,"amount"=>$amount,"msg"=>$msg];
        return $return_sts;

    }
}
